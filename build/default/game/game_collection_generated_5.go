embedded_components {
  id: "selection"
  type: "sprite"
  data: "tile_set: \"/game/game_atlas.atlas\"\ndefault_animation: \"selection\"\nmaterial: \"/builtins/materials/sprite.material\"\nblend_mode: BLEND_MODE_ALPHA\n"
  position {
    x: 411.0
    y: 160.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
